# README #

## Instructions

* The VbaChallenge.bas script will loop through all the stocks for one year and output the following information.

  * The ticker symbol.

  * Yearly change from opening price at the beginning of a given year to the closing price at the end of that year.

  * The percent change from opening price at the beginning of a given year to the closing price at the end of that year.

  * The total stock volume of the stock.

To run, first import VbaChallenge.bas then create the following macro 

```
Sub process()
    On Error Resume Next 
    Dim obj As New VbaChallenge
    obj.run
End Sub

```
The output should look as follows:

![hard_solution](Images/Capture2016.PNG)

