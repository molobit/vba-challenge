VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "VbaChallenge"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' Force defining variables mandatory
Option Explicit

' Data column/row number references
Const ID_DATA_COL As Integer = 1
Const TICKER_DATA_COL As Integer = 2
Const DATE_DATA_COL As Integer = 3
Const OPEN_DATA_COL As Integer = 4
Const HIGH_DATA_COL As Integer = 5
Const LOW_DATA_COL As Integer = 6
Const CLOSE_DATA_COL As Integer = 7
Const VOL_DATA_COL As Integer = 8

Const HEADER_ROW As Integer = 1

' Analysis Columns
Const TICKER_COL = VOL_DATA_COL + 2
Const YEAR_CHANGE_COL = TICKER_COL + 1
Const PERCENT_CHANGE_COL = YEAR_CHANGE_COL + 1
Const TOTAL_STOCK_VOL_COL = PERCENT_CHANGE_COL + 1

' Colors
Const RED_COLOR = 3
Const GREEN_COLOR = 4

' Range containing data for the purpose of analysis
Private DataRangeLookup As Range

Public Sub run()
    Application.ScreenUpdating = False
    Dim Ws As Worksheet
    For Each Ws In Worksheets
        ' Set current worksheet as active, module functions rely on active worksheets
        Ws.Activate
        If SheetNotProcessed Then
            CreateIdColumn
            TickerSummaryColumn
            CreateStatsColumns
            ProcessStats
        End If
    Next Ws
    Application.ScreenUpdating = True
End Sub

Private Sub TickerSummaryColumn():
    ' Define and set ticker title and ticker symbol range
    Dim LastColumn, LastRow As Long
    Dim TickerTitle As Range
    LastColumn = GetLastColumn()
    LastRow = GetLastRow()

    With ActiveSheet
        Dim TickerColumn, TickerRefColumn As Long
        TickerColumn = LastColumn + 2
        TickerRefColumn = 2

        ' The ticker column will be placed on the second column after the last existing column
        Set TickerTitle = ActiveSheet.Cells(1, TickerColumn)
        Dim LastCell As Range
        Set LastCell = ActiveSheet.Cells(1, LastColumn)

        ' Set title only if it has not been set
        ' as part of the check, we need to inspect the contents of the last cell
        If (TickerTitle.Value = "" And LastCell.Value <> "Ticker") Then
            ' Set column title
            TickerTitle.Value = "Ticker"

            ' Select and filter entries for <ticker> column
            ' Skip header field and select second row to the end of the last row
            Dim TickerValues As Range
            Set TickerValues = Range(Cells(2, TickerRefColumn), Cells(LastRow, TickerRefColumn))
            ' Copy Values and Clear duplicates
            Dim TargetColFirstCell, TargetColLastCell As Range
            Dim TargetColumn As Range
            Set TargetColFirstCell = Cells(2, TickerColumn)
            Set TargetColLastCell = Cells(LastRow, TickerColumn)
            Set TargetColumn = Range(TargetColFirstCell, TargetColLastCell)

            TickerValues.Copy TargetColumn
            TargetColumn.CurrentRegion.RemoveDuplicates Columns:=1, Header:=xlNo
        End If

    End With
End Sub

Private Sub CreateIdColumn():
    With ActiveSheet
        ' Create a reference column to be able to work with VLookUps
        If (Not ColumnHeaderExists(ID_DATA_COL, "<id>")) Then
            Dim LastRow As Long
            LastRow = GetLastRow
            ' Add a new column and assign a formula to create ids in the form of [ticker-date]
            Range("A1").EntireColumn.Insert
            Cells(1, 1).Value = "<id>"
            Cells(2, 1).Formula = "=B2&-C2"
            Cells(2, 1).Copy
            ' Apply formula to all rows in first column
            Range(Cells(3, 1), Cells(LastRow, 1)).PasteSpecial (xlPasteAll)
        End If

        ' Define data range for vlookups
        Set DataRangeLookup = Range(Cells((HEADER_ROW + 1), ID_DATA_COL), Cells(GetLastRow, VOL_DATA_COL))

    End With
End Sub

' Create Stats Columns
Private Sub CreateStatsColumns():

    With ActiveSheet
        Dim YearlyChangeTitleCell, PercentChangeCell, TotalStockVolCell As Range
        Set YearlyChangeTitleCell = Cells(HEADER_ROW, YEAR_CHANGE_COL)
        Set PercentChangeCell = Cells(HEADER_ROW, PERCENT_CHANGE_COL)
        Set TotalStockVolCell = Cells(HEADER_ROW, TOTAL_STOCK_VOL_COL)

        If (Not ColumnHeaderExists(YEAR_CHANGE_COL, "Yearly Change")) Then
            YearlyChangeTitleCell.Value = "Yearly Change"
        End If

        If (Not ColumnHeaderExists(PERCENT_CHANGE_COL, "Percent Change")) Then
            PercentChangeCell.Value = "Percent Change"
        End If

        If (Not ColumnHeaderExists(TOTAL_STOCK_VOL_COL, "Total Stock Volume")) Then
            TotalStockVolCell.Value = "Total Stock Volume"
        End If
    End With
End Sub

Private Sub ProcessStats():
    Dim ActiveTicker As Range
    For Each ActiveTicker In Range(Cells(HEADER_ROW + 1, TICKER_COL), Cells(GetLastRow, TICKER_COL))
        If ActiveTicker.Value <> "" Then
            Dim OpenVal, CloseVal As Double
            OpenVal = GetBeginingOfYearOpenValue(ActiveTicker.Value)
            CloseVal = GetEndOfYearCloseValue(ActiveTicker.Value)

            ' Calculate Yearly Change
            ActiveTicker.Cells(1, 2).Value = GetYearlyChange(OpenVal, CloseVal)
            If (ActiveTicker.Cells(1, 2).Value > 0) Then
                ActiveTicker.Cells(1, 2).Interior.ColorIndex = GREEN_COLOR
            Else
                ActiveTicker.Cells(1, 2).Interior.ColorIndex = RED_COLOR
            End If

            ' Calculate Percent Change
            ActiveTicker.Cells(1, 3).Value = GetYearPercentChange(OpenVal, CloseVal)
            ActiveTicker.Cells(1, 3).NumberFormat = "00.0%"

            ' Calculate Total Stock Volume
            ActiveTicker.Cells(1, 4).Value = GetTotalStockVolume(ActiveTicker.Value)
        Else
            Exit For
        End If
    Next ActiveTicker
End Sub

' Calculate yearly change from opening price at the beginning of a given year to the closing price at the end of that year.
Private Function GetYearlyChange(ByVal OpenVal As Double, ByVal CloseVal As Double) As Double:
    GetYearlyChange = CloseVal - OpenVal
End Function

' Calculate percent change for a given ticker
Private Function GetYearPercentChange(ByVal OpenVal As Double, ByVal CloseVal As Double) As Double:
    GetYearPercentChange = ((CloseVal - OpenVal) / Abs(OpenVal))
End Function

' Calculate total stock volume by ticker
Private Function GetTotalStockVolume(ByVal Ticker As String) As LongLong
    Dim VolRange, TickerRange As Range

    Set VolRange = Range(Cells(HEADER_ROW + 1, VOL_DATA_COL), Cells(GetLastRow, VOL_DATA_COL))
    Set TickerRange = Range(Cells(HEADER_ROW + 1, TICKER_DATA_COL), Cells(GetLastRow, TICKER_DATA_COL))

    With WorksheetFunction
        GetTotalStockVolume = .SumIfs(VolRange, TickerRange, Ticker)
    End With
End Function

Private Function GetBeginingOfYearOpenValue(ByVal Ticker As String) As Double
    Dim StartDate As LongLong
    Dim TickerStart As String
    StartDate = GetMinDateByTicker(Ticker)

    With WorksheetFunction
        TickerStart = Ticker & "-" & .Trim(Str(StartDate))
        GetBeginingOfYearOpenValue = .VLookup(TickerStart, DataRangeLookup, OPEN_DATA_COL, False)
    End With
End Function

Private Function GetEndOfYearCloseValue(ByVal Ticker As String) As Double
    Dim EndDate As LongLong
    Dim TickerEnd As String
    EndDate = GetMaxDateByTicker(Ticker)

    With WorksheetFunction
        TickerEnd = Ticker & "-" & .Trim(Str(EndDate))
        GetEndOfYearCloseValue = .VLookup(TickerEnd, DataRangeLookup, CLOSE_DATA_COL, False)
    End With
End Function

' Check if column exists
Private Function ColumnHeaderExists(ByVal ColumnNumber As Integer, ByVal ColumnVal As String) As Boolean:
    Dim ActiveColumn As Range
    Set ActiveColumn = Cells(HEADER_ROW, ColumnNumber)

    ColumnHeaderExists = (ActiveColumn.Value <> "" And ActiveColumn.Value = ColumnVal)
End Function

' Get last date for a given ticker
Private Function GetMaxDateByTicker(ByVal Ticker As String) As LongLong
    Dim DateRange, TickerRange As Range

    Set DateRange = Range(Cells(HEADER_ROW + 1, DATE_DATA_COL), Cells(GetLastRow, DATE_DATA_COL))
    Set TickerRange = Range(Cells(HEADER_ROW + 1, TICKER_DATA_COL), Cells(GetLastRow, TICKER_DATA_COL))

    With WorksheetFunction
        GetMaxDateByTicker = .MaxIfs(DateRange, TickerRange, Ticker)
    End With
End Function

' Get first date for any given ticker
Private Function GetMinDateByTicker(ByVal Ticker As String) As LongLong
    Dim DateRange, TickerRange As Range

    Set DateRange = Range(Cells(HEADER_ROW + 1, DATE_DATA_COL), Cells(GetLastRow, DATE_DATA_COL))
    Set TickerRange = Range(Cells(HEADER_ROW + 1, TICKER_DATA_COL), Cells(GetLastRow, TICKER_DATA_COL))

    With WorksheetFunction
        GetMinDateByTicker = .MinIfs(DateRange, TickerRange, Ticker)
    End With
End Function

' Get last row of current worksheet
Private Function GetLastRow() As Long:
    Dim LastRow As Long
    With ActiveSheet
        LastRow = ActiveSheet.Cells(Rows.Count, 1).End(xlUp).Row
    End With

    GetLastRow = LastRow
End Function

' Get last column of current worksheet
Private Function GetLastColumn() As Long:
    Dim LastColumn As Long
    With ActiveSheet
        LastColumn = ActiveSheet.Cells(1, ActiveSheet.Columns.Count).End(xlToLeft).Column
    End With

    GetLastColumn = LastColumn
End Function

' Determine if we should process active sheet
Private Function SheetNotProcessed() As Boolean:
    SheetNotProcessed = Not ColumnHeaderExists(ID_DATA_COL, "<id>") And _
                        Not ColumnHeaderExists(TICKER_COL, "Ticker") And _
                        Not ColumnHeaderExists(YEAR_CHANGE_COL, "Yearly Change") And _
                        Not ColumnHeaderExists(PERCENT_CHANGE_COL, "Percent Change") And _
                        Not ColumnHeaderExists(TOTAL_STOCK_VOL_COL, "Total Stock Volume")

End Function
